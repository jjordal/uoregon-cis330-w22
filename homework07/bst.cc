#include "bst.h"

// ---------------------------------------
// Node class
// Default constructor
Node::Node() {
	this->right = NULL;
	this->left = NULL;
	this->parent = NULL;
	this->key = 0;	
}
// Constructor
Node::Node(int in) {
	this->key = in;
	this->parent = NULL;
	this->right = NULL;
	this->left = NULL;
}
// Destructor
Node::~Node() {
}

// Add parent 
void Node::add_parent(Node* in) {
	this->parent = in;
}
// Add to left of current node
void Node::add_left(Node* in) {
	this->left = in;
}
// Add to right of current node
void Node::add_right(Node* in) {
	this->right = in;
}

// Get key
int Node::get_key()
{
	return this->key;
}
// Get parent node
Node* Node::get_parent()
{
	return this->parent;
}
// Get left node
Node* Node::get_left()
{
	return this->left;
}
// Get right node
Node* Node::get_right()
{
	return this->right;
}
// Print the key to ostream to
// Do not change this
void Node::print_info(ostream& to)
{
    to << key << endl;
}
// ---------------------------------------


// ---------------------------------------
// BST class
// Walk the subtree from the given node
void BST::inorder_walk(Node* in, ostream& to)
{
	if(in != NULL){
		this->inorder_walk(in->get_left(), to);
		in->print_info(to);
		this->inorder_walk(in->get_right(), to);
	}
}
// Constructor
BST::BST()
{
	this->root = NULL;
}
// Destructor
BST::~BST()
{
	while(this->root != NULL){
		this->delete_node(this->root);
	}	
}
// Insert a node to the subtree
void BST::insert_node(Node* in)
{	
	if(this->root == NULL){
		this->root = in;
		return;
	}	
	Node* temp = this->root;
	while(true){
		if(temp->get_key() > in->get_key()){
			if(temp->get_left() == NULL){
				break;
			}
			temp = temp->get_left();
		}else{
			if(temp->get_right() == NULL){
				break;
			}	
			temp = temp->get_right();
		}
	}
	in->add_parent(temp);
	if(temp->get_key() > in->get_key()){
		temp->add_left(in);
	}else{
		temp->add_right(in);
	}
}
// Delete a node to the subtree
void BST::delete_node(Node* out)
{
	if(out->get_right() == NULL && out->get_left() == NULL){
		if(out == this->root){
			this->root = NULL;
		}else if(out->get_parent()->get_left() == out){
			out->get_parent()->add_left(NULL);
		}else{
			out->get_parent()->add_right(NULL);
		}
		delete out;
	}else if(out->get_right() != NULL && out->get_left() != NULL){
		Node* succ = this->get_succ(out);
		int val = succ->get_key();
		Node* temp = new Node(val);
		temp->add_right(out->get_right());
		temp->add_left(out->get_left());
		temp->add_parent(out->get_parent());
		out->get_right()->add_parent(temp);
		out->get_left()->add_parent(temp);
		this->delete_node(succ);
		if(out == this->root){
			this->root = temp;
		}else if(out == out->get_parent()->get_left()){
			out->get_parent()->add_left(temp);
		}else{
			out->get_parent()->add_right(temp);
		}
		
		delete out;		

	}else{
		Node* child;
		if(out->get_right() != NULL){
			child = out->get_right();
		}else{
			child = out->get_left();
		}

		if(out == this->root){
			this->root = child;
		}else if(out == out->get_parent()->get_left()){
			out->get_parent()->add_left(child);	
		}else{
			out->get_parent()->add_right(child);
		}
		child->add_parent(out->get_parent());
		delete out;
	}
}
// minimum key in the BST
Node* BST::tree_min()
{
	Node* temp = this->root;
	while(temp->get_left() != NULL){
		temp = temp->get_left();
	}
	return temp;
}
// maximum key in the BST
Node* BST::tree_max()
{
	Node* temp = this->root;
	while(temp->get_right() != NULL){
		temp = temp->get_right();
	}
	return temp;
}
	// Get the minimum node from the subtree of given node
Node* BST::get_min(Node* in)
{
	Node* temp = in;
	while(temp->get_left() != NULL){
		temp = temp->get_left();
	}
	return temp;
}
// Get the maximum node from the subtree of given node
Node* BST::get_max(Node* in)
{
	Node* temp = in;
	while(temp->get_right() != NULL){
		temp = temp->get_right();
	}
	return temp;
}
// Get successor of the given node
Node* BST::get_succ(Node* in)
{
	if(in->get_right() != NULL){
		return this->get_min(in->get_right());
	}

	Node* parent = in->get_parent();
	while(parent != NULL && in == parent->get_right()){
		in = parent;
		parent = parent->get_parent();
	}
	return parent;
}
// Get predecessor of the given node
Node* BST::get_pred(Node* in)
{
	if(in->get_left() != NULL){
		return this->get_max(in->get_left());
	}

	Node* parent = in->get_parent();
	while(parent != NULL && in == parent->get_left()){
		in = parent;
		parent = parent->get_parent();
	}
	return parent;
}
// Walk the BST from min to max
void BST::walk(ostream& to)
{
	this->inorder_walk(this->root, to);
}
// Search the tree for a given key
Node* BST::tree_search(int search_key)
{
	Node* temp = this->root;
	while(temp != NULL){
		if(temp->get_key() == search_key){
			return temp;
		}else if(temp->get_key() < search_key){
			temp = temp->get_right();
		}else{
			temp = temp->get_left();
		}
	}
	return temp;
}
// ---------------------------------------
