#include <string>
#include <iostream>
#include "rcipher.h"


// -------------------------------------------------------
// ROT13 Cipher implementation
const string alphabet = "abcdefghijklmnopqrstuvwxyz";
struct RCipher::CipherCheshire{
	string key;
};

RCipher::RCipher(){
	smile = new CipherCheshire;
	string temp = alphabet;
	rotate_string(temp, 13);
	smile->key = temp;
}



RCipher::~RCipher(){
	delete smile;
}

string RCipher::encrypt(string raw){
	int len = raw.size();
	int index;
	string ret;
	char tmp;
	for(int i = 0; i < len; i++){
		index = find_pos(alphabet, raw[i]);
		if(index != -1){
			tmp = smile->key[index];
			if(isupper(raw[i])){
				ret += toupper(tmp);
			}else{
				ret += tmp;
			}
		}else{
			ret += " ";
		}
	}
	return ret;
}
string RCipher::decrypt(string enc){
	int len = enc.size();
	int index;
	string ret;
	char tmp;
	for(int i = 0; i < len; i++){
		index = find_pos(smile->key, enc[i]);
		if(index != -1){
			tmp = alphabet[index];
			if(isupper(enc[i])){
				ret += toupper(tmp);
			}else{
				ret += tmp;
			}
		}else{
			ret += " ";
		}
	}
	return ret;
}

