#include <string>
#include <iostream>
#include <vector>
#include "ctype.h"
#include "kcipher.h"


// -------------------------------------------------------
// Runnning-Key Cipher implementation
const string alphabet = "abcdefghijklmnopqrstuvwxyz";
struct KCipher::CipherCheshire{
	vector<string> key;
	int page = 0;
};
KCipher::KCipher(){
	smile = new CipherCheshire;
	char a = 'a';	
	string same = string(MAX_LENGTH, a);
	smile->key.push_back(same);
}
KCipher::KCipher(string key){
	smile = new CipherCheshire;
	int len = key.size();
	for(int i = 0; i < len; i++){
		if(!isalpha(key[i]) && key[i] != ' '){
			cerr << "Invalid Running key: " << key << endl;
		}
	}
	if(len == 0){
		cerr << "Invalid Running key: " << key << endl;
	}
	smile->key.push_back(key);
}

void KCipher::add_key(string key){	
	int len = key.size();
	for(int i = 0; i < len; i++){
		if(!isalpha(key[i]) && key[i] != ' '){
			cerr << "Invalid Running key: " << key << endl;
		}
	}
	if(len == 0){
		cerr << "Invalid Running key: " << key << endl;
	}
	smile->key.push_back(key);
}

void KCipher::set_id(int page){
	int pages = smile->key.size();
	if(page > pages){
		cerr << "Warning: invalid id: " << page << endl;
	}
	smile->page = page;
}

KCipher::~KCipher(){
	 delete smile;
}
string KCipher::encrypt(string raw){
	int len = raw.size();
	int index;
	string ret;
	char tmp;
	int ctr = 0;
	string key = smile->key[smile->page];
	for(int i = 0; i < len; i++){
		string temp_alpha = alphabet;
		index = find_pos(alphabet, raw[i]);
		if(index != -1){
			rotate_string(temp_alpha, index);
			while(isalpha(key[ctr]) == 0){
				ctr++;
			}
			index = find_pos(alphabet, key[ctr]);
			tmp = temp_alpha[index];
			ctr++;
			if(isupper(raw[i])){
				ret += toupper(tmp);
			}else{
				ret += tmp;
			}
		}else{
			ret += " ";
		}
	}
	return ret;
}
string KCipher::decrypt(string enc){
	int len = enc.size();
	int ctr = 0;
	int index = -1;	
	string ret;
	char tmp;
	string key = smile->key[smile->page];
	for(int i = 0; i < len; i++){
		if(isalpha(enc[i])){
			string temp_alpha = alphabet;
			index = find_pos(alphabet, key[ctr]);
			while(index == -1){
				ctr++;
				index = find_pos(alphabet, key[ctr]);
			}
			rotate_string(temp_alpha, index);
			index = find_pos(temp_alpha, enc[i]);
			tmp = alphabet[index];
			ctr++;
			if(isupper(enc[i])){
				ret += toupper(tmp);
			}else{
				ret += tmp;
			}
		}else{
			ret += " ";
		}
	}
	return ret;
}


