#include <string>
#include <iostream>
#include <algorithm>
#include "ccipher.h"


// -------------------------------------------------------
// Caesar Cipher implementation
const string alphabet = "abcdefghijklmnopqrstuvwxyz";
struct CCipher::CipherCheshire{
	int rotation;
};
CCipher::CCipher(){
	smile = new CipherCheshire;
	smile->rotation = 0;
}
CCipher::CCipher(int rotation){
	if(rotation < 0){
		cerr << "Error: Caeser cipher is less than 0" << endl;
	}
	smile = new CipherCheshire;
	smile->rotation = rotation;
}

CCipher::~CCipher(){
	delete smile;
}
string CCipher::encrypt(string raw){
	string a_temp = alphabet;
	rotate_string(a_temp, smile->rotation);
	int len = raw.size();
	int index;
	string ret;
	char tmp;
	for(int i = 0; i < len; i++){
		index = find_pos(alphabet, raw[i]);
		if(index != -1){
			tmp = a_temp[index];
			if(isupper(raw[i])){
				ret += toupper(tmp);
			}else{
				ret += tmp;
			}
		}else{
			ret += " ";
		}
	}
	return ret;
}
string CCipher::decrypt(string enc){
	string a_temp = alphabet;
	rotate_string(a_temp, smile->rotation);
	int len = enc.size();
	int index;
	string ret;
	char tmp;
	for(int i = 0; i < len; i++){
		index = find_pos(a_temp, enc[i]);
		if(index != -1){
			tmp = alphabet[index];
			if(isupper(enc[i])){
				ret += toupper(tmp);
			}else{
				ret += tmp;
			}
		}else{
			ret += " ";
		}
	}
	return ret;
}
// -------------------------------------------------------


// Rotates the input string in_str by rot positions
void rotate_string(string& in_str, int rot)
{
    char temp;
    int len = in_str.size();
    rot = rot % 26;
    int split = len - rot;
    len--;
    for(int i = 0; i < rot / 2; i++){
	temp = in_str[i];
	in_str[i] = in_str[(rot - 1) - i];
	in_str[(rot - 1) - i] = temp;
    }
    for(int i = 0; i < split / 2; i++){
	temp = in_str[rot + i];
	in_str[rot + i] = in_str[len - i];
	in_str[len - i] = temp;
    }
    for(int i = 0; i <= len / 2; i++){
	temp = in_str[i];
	in_str[i] = in_str[len-i];
	in_str[len-i] = temp;
    }
}
