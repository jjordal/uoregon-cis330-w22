#include <string>
#include <iostream>
#include <vector>
#include "kcipher.h"
#include "vcipher.h"


// -------------------------------------------------------
// Running Key Cipher implementation
// -------------------------------------------------------
const string alphabet = "abcdefghijklmnopqrstuvwxyz";
struct VCipher::CipherCheshire{
	string key;
};

VCipher::VCipher(){
	smile = new CipherCheshire;
	char a = 'a';
	smile->key = string(MAX_LENGTH, a);
}

VCipher::VCipher(string key){
	smile = new CipherCheshire;
	int len = key.size();
	for(int i = 0; i < len; i++){
		if(!isalpha(key[i]) || !islower(key[i])){
			cerr << "Error: not a valid Vigenere key word" << endl;
		}
	}
	smile->key = key;
}

VCipher::~VCipher(){
	delete smile;
}

string VCipher::encrypt(string raw){
	int len = raw.size();
	int index;
	string ret;
	char tmp;
	int ctr = 0;
	string key = smile->key;
	int key_len = key.size();
	for(int i = 0; i < len; i++){
		string temp_alpha = alphabet;
		index = find_pos(alphabet, raw[i]);
		if(index != -1){
			rotate_string(temp_alpha, index);
			if(ctr >= key_len){
				ctr = 0;
			}
			index = find_pos(alphabet, key[ctr]);
			tmp = temp_alpha[index];
			ctr++;
			if(isupper(raw[i])){
				ret += toupper(tmp);
			}else{
				ret += tmp;
			}
		}else{
			ret += " ";
		}
	}
	return ret;
}

string VCipher::decrypt(string enc){
	string ret;	
	int len = enc.size();
	int index;
	char tmp;
	int ctr = 0;
	string key = smile->key;
	int key_len = key.size();
	for(int i = 0; i < len; i++){
		if(isalpha(enc[i])){
			string temp_alpha = alphabet;
			if(ctr >= key_len){
				ctr = 0;
			}
			index = find_pos(alphabet, key[ctr]);
			rotate_string(temp_alpha, index);
			index = find_pos(temp_alpha, enc[i]);
			tmp = alphabet[index];
			ctr++;

			if(isupper(enc[i])){
				ret += toupper(tmp);
			}else{
				ret += tmp;
			}
		}else{
			ret += " ";
		}
	}
	return ret;
}	
