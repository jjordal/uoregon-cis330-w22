#ifndef KCIPHER_H_
#define KCIPHER_H_
#include "cipher.h"
#include "ccipher.h"

using namespace std;

const unsigned int MAX_LENGTH = 100;

/* A class that implements a
   Running key cipher class. It 
   inherits the Cipher class */
class KCipher : public Cipher {
	protected:
		struct CipherCheshire;
		CipherCheshire *smile;
	public:
		KCipher();
		KCipher(string key);
		~KCipher();
		void add_key(string key);
		void set_id(int page);
		virtual string encrypt(string raw);
		virtual string decrypt(string enc);

};


#endif

